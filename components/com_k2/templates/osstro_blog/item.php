<?php defined('_JEXEC') or die;

// Set Extra To Names
$Extra_Fields = [];
foreach ($this->item->extra_fields as $Key => $Value) {
    $Extra_Fields[$Value->alias] = $Value->value;
}
?>

<article class="Blog-Post">
    <div class="Blog-Post-Image">
        <div>
            <?php if (isset($Extra_Fields['Letters'])): ?>
                <div class="Blog-Post-Letters Script-Vertical-Align" data-max-width="1000">
                    <?php echo $Extra_Fields['Letters'] ?>
                </div>
            <?php endif; ?>
            <img src="<?php echo $this->item->image; ?>">
        </div>
        <header>
            <h2 class="Blog-Post-Title">
                <?php echo $this->item->title; ?>
            </h2>

            <p class="Blog-Post-Date"><?= str_replace(date("Y"), '', JHTML::_('date', $this->item->created, 'j F Y')) ?>  </p>
        </header>
    </div>

    <div class="Blog-Post-Text">
        <?php echo $this->item->introtext; ?>
        <?php echo $this->item->fulltext; ?>
    </div>

    <div class="Blog-Post-Meta">
        <span class="Blog-Post-Author"><?= $this->item->author->name; ?></span>
        <span class="Blog-Post-Tags">
            <?php foreach ($this->item->tags as $Tag_Index => $Tag) { ?><?= ($Tag_Index > 0) ? ', ' : '' ?><?= $Tag->name; ?><?php } ?>
        </span>
        <span class="Blog-Post-Date"><?= JHTML::_('date', $this->item->created, 'j F Y') ?></span>
    </div>

    <div class="Blog-Post-Social">

        <div class="Share-Links">
            <div class='like-block Share-Facebook'>
                <span class='like-button share s_facebook'></span>
                <span class='like-counter counter c_facebook'></span>
            </div>

            <div class='like-block Share-Twitter'>
                <span class='like-button share s_twitter'></span>
                <span class='like-counter counter c_twitter'></span>
            </div>

            <div class='like-block Share-Vk'>
                <span class='like-button share s_vk'></span>
                <span class='like-counter counter c_vk'></span>
            </div>
        </div>


        <!-- Facebook Button -->
        <div class="Like-Facebook" id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_EN/sdk.js#xfbml=1&version=v2.5&appId=335556789884889";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        <div class="fb-like" data-href="<?php echo $this->item->link; ?>" data-width="40px"
             data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
        <!-- Facebook Button -->

    </div>

</article>

<section class="After-Component">

    <?php
    // Render modules inside this article override
    $doc = JFactory::getDocument();
    $renderer = $doc->loadRenderer("modules");
    $raw = ['style' => 'standard'];
    // Echo the module somewhere in the template
    echo $renderer->render("Same-Post-Letter", $raw, null);
    ?>
    <div class="Blog-Same-Posts">
        <?php foreach ($this->relatedItems as $key => $item): ?>
            <a class="Blog-Same-Post" href="<?php echo $item->link ?>">
                <header>
                    <h2 class="Blog-Post-Title">
                        <?php echo $item->title; ?>
                    </h2>

                    <p class="Blog-Post-Date"><?= str_replace(date("Y"), '', JHTML::_('date', $this->item->created, 'j F Y')) ?></p>
                </header>
                <div class="Blog-Same-Post-Image">
                    <img src="<?php echo $item->image; ?>">
                </div>

            </a>
        <?php endforeach; ?>
    </div>
</section>