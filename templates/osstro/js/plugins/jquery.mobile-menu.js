(function ($) {
    $(document).ready(function () {

        var $Hamburger = $('.Hamburger'),
            $Menu = $('.Mobile-Menu .Menu'),
            Duration = 600;

        $Hamburger.click(function () {
            $Menu.slideToggle(Duration);
            $Menu.parents('nav').toggleClass('Active');
            return false;
        });

        $('body').click(function () {
            $Menu.slideUp(Duration);
            $Menu.parents('nav').removeClass('Active');
        });

    });
})(jQuery);