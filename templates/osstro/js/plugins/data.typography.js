/*
 * Copyright (c) 2015
 * Data Scripts - Typography
 * Version 1.0.0
 * Create 2015.12.04
 * Author Bunker Labs

 * Usage:
 * Add class name 'Script-Typography' - to set Block with content
 *  data-typography-lang              - to set language (*ru | en)

 * Code structure:
 * <div class="Script-Typography" data-typography-lang='en'>
 *      <h3>Title</h3>
 *      <p>Description and comments</p>
 * </div>
 */
(function ($) {
    $(document).ready(function () {
        var $Typography = $('.Script-Typography'),
            $Typography_Elements = $Typography.find('h1,h2,h3,h4,h5,h6,p,li,a'),
            Lang = $Typography.attr('data-typography-lang') ? $Typography.attr('data-typography-lang') : 'ru',
            Post = [],
            Pre = [];

        if (Lang == 'ru') {
            Pre = [
                // Prepositions
                'в', 'без', 'до', 'из', 'к', 'на', 'по', 'о', 'от', 'перед', 'при',
                'через', 'с', 'у', 'за', 'над', 'об', 'под', 'про', 'для',
                'вблизи', 'вглубь', 'вдоль', 'возле', 'около', 'вокруг', 'впереди', 'после',
                'посредством', 'путём', 'насчёт', 'ввиду',
                'благодаря', 'несмотря на', 'спустя',
                'из-под', 'из-за', 'по-над',

                // Unions
                'а', 'и', 'вдобавок', 'же', 'едва', 'если', 'зато', 'зачем', 'как', 'когда', 'либо', 'однако', 'но', 'ни',
                'пока', 'почему', 'чем', 'что', 'чтоб', 'чтобы',
                // Currency
                '$'
            ];


            Post = [
                // Reductions
                'г.', 'гг.', 'тыс.', 'млн.', 'млрд.', 'трлн.', 'с.', 'чел.', 'м.', 'кг.',
                // Currency
                '₽', 'руб.'
            ];
        } else if (Lang == 'en') {
            Pre = [
                // Prepositions
                'aboard', 'about', 'above', 'absent', 'across', 'afore', 'in', 'down', 'for',
                'from', 'into', 'like', 'minus', 'near', 'on', 'of', 'than', 'till',

                // Unions
                'and', 'but', 'or', 'whereas', 'while', 'that', 'if', 'after', 'as', 'before',
                'since', 'when', 'while', 'because', 'for', 'that', 'lest',
                // Currency
                '$'
            ];

            Post = [
                // Reductions
                'pp.', 'cur.', 'sig.', 'adsd'
                // Currency
            ];
        }

        $Typography_Elements.each(function () {
            var Content = $(this).html();
            for (var Index = 0; Index < Pre.length; Index++) {
                Content = Content.replace(' ' + Pre[Index] + ' ', ' ' + Pre[Index] + '&nbsp;');
            }

            for (Index = 0; Index < Pre.length; Index++) {
                Content = Content.replace(' ' + Post[Index] + ' ', '&nbsp;' + Post[Index] + ' ');
            }

            $(this).html(Content);
        });


    });
})(jQuery);