(function ($) {
    $(document).ready(function () {
        var $Modal_Outer = $('.Modal-Outer'),
            $Modal_Window = $('.Modal'),
            $Close_Item = $('.Modal-Outer, .Close'),
            $Activator = $('.Dialog .Button-1');


        $Activator.click(function () {
            $Modal_Outer.fadeIn();
        });

        $Close_Item.click(function () {
            $Modal_Outer.fadeOut();
        });

        $Modal_Window.click(function () {
            return false;
        });
    });
})(jQuery);