<?php defined('_JEXEC') or die; ?>

<section class="Comments">
    <?php foreach ($this->leading as $key => $item): ?>
        <?php
        $this->item = $item;
        echo $this->loadTemplate('item');
        ?>
    <?php endforeach; ?>
</section>
