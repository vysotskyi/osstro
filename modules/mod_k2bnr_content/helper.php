<?php
/**
 * @version        $Id: helper.php $
 * @package        K2
 * @author        BNR Branding Solutions, http://www.bnrbranding.com
 * @copyright    Copyright (C) 2010 BNR Branding Solutions. All rights reserved.
 * @license        GNU General Public License <http://www.gnu.org/copyleft/gpl.html>
 * @link        http://www.bnrbranding.com
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

require_once(JPATH_SITE . '/components/com_k2/helpers/route.php');
require_once(JPATH_SITE . '/components/com_k2/helpers/utilities.php');

class modK2BNRContentHelper
{

    function getItems(&$params, $format = 'html')
    {
        jimport('joomla.filesystem.file');

        $input = & JFactory::getApplication()->input;
        $componentParams = & JComponentHelper::getParams('com_k2');
        $limitstart = 0;

        // Get current K2 item
        $id = $input->getInt('id', 0);
        $db = & JFactory::getDBO();
        $query = $db->getQuery(true);
        $query
            ->select('i.*, c.name AS categoryname,c.id AS categoryid, c.alias AS categoryalias, c.params AS categoryparams')
            ->from('#__k2_items as i')
            ->join('LEFT', '#__k2_categories c ON c.id = i.catid')
            ->where("i.id={$id}");

        $db->setQuery($query, 0, 1);
        $item = $db->loadObject();
        $model = K2Model::getInstance('Item', 'K2Model');

        //Clean title
        $item->title = JFilterOutput::ampReplace($item->title);

        //Images
        if ($params->get('bnrItemImage')) {

            if (JFile::exists(JPATH_SITE . '/media/k2/items/cache/' . md5("Image" . $item->id) . '_XS.jpg')) {
                $item->imageXSmall = JURI::base(true) . '/media/k2/items/cache/' . md5("Image" . $item->id) . '_XS.jpg';
            }

            if (JFile::exists(JPATH_SITE . '/media/k2/items/cache/' . md5("Image" . $item->id) . '_S.jpg')) {
                $item->imageSmall = JURI::base(true) . '/media/k2/items/cache/' . md5("Image" . $item->id) . '_S.jpg';
            }

            if (JFile::exists(JPATH_SITE . '/media/k2/items/cache/' . md5("Image" . $item->id) . '_M.jpg')) {
                $item->imageMedium = JURI::base(true) . '/media/k2/items/cache/' . md5("Image" . $item->id) . '_M.jpg';
            }

            if (JFile::exists(JPATH_SITE . '/media/k2/items/cache/' . md5("Image" . $item->id) . '_L.jpg')) {
                $item->imageLarge = JURI::base(true) . '/media/k2/items/cache/' . md5("Image" . $item->id) . '_L.jpg';
            }

            if (JFile::exists(JPATH_SITE . '/media/k2/items/cache/' . md5("Image" . $item->id) . '_XL.jpg')) {
                $item->imageXLarge = JURI::base(true) . '/media/k2/items/cache/' . md5("Image" . $item->id) . '_XL.jpg';
            }

            if (JFile::exists(JPATH_SITE . '/media/k2/items/cache/' . md5("Image" . $item->id) . '_Generic.jpg')) {
                $item->imageGeneric = JURI::base(true) . '/media/k2/items/cache/' . md5("Image" . $item->id) . '_Generic.jpg';
            }

            $image = 'image' . $params->get('bnrItemImgSize', 'Small');
            if (isset($item->$image))
                $item->image = $item->$image;

        }

        //Read more link
        $item->link = urldecode(JRoute::_(K2HelperRoute::getItemRoute($item->id . ':' . urlencode($item->alias), $item->catid . ':' . urlencode($item->categoryalias))));

        //Tags
        if ($params->get('bnrItemTags')) {
            $tags = $model->getItemTags($item->id);
            for ($i = 0; $i < sizeof($tags); $i++) {
                $tags[$i]->link = JRoute::_(K2HelperRoute::getTagRoute($tags[$i]->name));
            }
            $item->tags = $tags;
        }

        //Category link
        if ($params->get('bnrItemCategory'))
            $item->categoryLink = urldecode(JRoute::_(K2HelperRoute::getCategoryRoute($item->catid . ':' . urlencode($item->categoryalias))));

        //Extra fields
        if ($params->get('bnrItemExtraFields')) {
            $item->extra_fields = $model->getItemExtraFields($item->extra_fields, $item);
        }

        //Comments counter
        if ($params->get('bnrItemCommentsCounter'))
            $item->numOfComments = $model->countItemComments($item->id);

        //Attachments
        if ($params->get('bnrItemAttachments'))
            $item->attachments = $model->getItemAttachments($item->id);

        //Import plugins
        if ($format != 'feed') {
            $dispatcher = JDispatcher::getInstance();
            JPluginHelper::importPlugin('content');
        }

        //Video
        if ($params->get('bnrItemVideo') && $format != 'feed') {
            $params->def('vwidth', $params->get('bnrItemVideoWidth', 400));
            $params->def('vheight', $params->get('bnrItemVideoHeight', 300));
            $params->set('vfolder', 'media/k2/videos');
            $params->set('afolder', 'media/k2/audio');
            $item->text = $item->video;
            if (K2_JVERSION == '15') {
                $dispatcher->trigger('onPrepareContent', array(&$item, &$params, $limitstart));
            } else {
                $dispatcher->trigger('onContentPrepare', array('mod_k2_content.', &$item, &$params, $limitstart));
            }
            $item->video = $item->text;
        }

        // Introtext
        $item->text = '';
        if ($params->get('bnrItemIntroText')) {
            // Word limit
            if ($params->get('bnrItemIntroTextWordLimit')) {
                $item->text .= K2HelperUtilities::wordLimit($item->introtext, $params->get('bnrItemIntroTextWordLimit'));
            } else {
                $item->text .= $item->introtext;
            }
        }

        if ($format != 'feed') {

            $params->set('parsedInModule', 1);
            // for plugins to know when they are parsed inside this module

            if ($params->get('JPlugins', 1)) {
                //Plugins
                if (K2_JVERSION != '15') {

					$item->event = new stdClass;
					if (!is_object($item)) {$item = new stdClass;} 
					
					$item->event->BeforeDisplay = '';
                    $item->event->AfterDisplay = '';

                    $dispatcher->trigger('onContentPrepare', array('mod_k2_content', &$item, &$params, $limitstart));

                    $results = $dispatcher->trigger('onContentAfterTitle', array('mod_k2_content', &$item, &$params, $limitstart));
                    $item->event->AfterDisplayTitle = trim(implode("\n", $results));

                    $results = $dispatcher->trigger('onContentBeforeDisplay', array('mod_k2_content', &$item, &$params, $limitstart));
                    $item->event->BeforeDisplayContent = trim(implode("\n", $results));

                    $results = $dispatcher->trigger('onContentAfterDisplay', array('mod_k2_content', &$item, &$params, $limitstart));
                    $item->event->AfterDisplayContent = trim(implode("\n", $results));
                } else {
                    $results = $dispatcher->trigger('onBeforeDisplay', array(&$item, &$params, $limitstart));
                    $item->event->BeforeDisplay = trim(implode("\n", $results));

                    $results = $dispatcher->trigger('onAfterDisplay', array(&$item, &$params, $limitstart));
                    $item->event->AfterDisplay = trim(implode("\n", $results));

                    $results = $dispatcher->trigger('onAfterDisplayTitle', array(&$item, &$params, $limitstart));
                    $item->event->AfterDisplayTitle = trim(implode("\n", $results));

                    $results = $dispatcher->trigger('onBeforeDisplayContent', array(&$item, &$params, $limitstart));
                    $item->event->BeforeDisplayContent = trim(implode("\n", $results));

                    $results = $dispatcher->trigger('onAfterDisplayContent', array(&$item, &$params, $limitstart));
                    $item->event->AfterDisplayContent = trim(implode("\n", $results));

                    $dispatcher->trigger('onPrepareContent', array(&$item, &$params, $limitstart));
                }

            }
            //Init K2 plugin events
            $item->event->K2BeforeDisplay = '';
            $item->event->K2AfterDisplay = '';
            $item->event->K2AfterDisplayTitle = '';
            $item->event->K2BeforeDisplayContent = '';
            $item->event->K2AfterDisplayContent = '';
            $item->event->K2CommentsCounter = '';

            if ($params->get('K2Plugins', 1)) {
                //K2 plugins
                JPluginHelper::importPlugin('k2');
                $results = $dispatcher->trigger('onK2BeforeDisplay', array(&$item, &$params, $limitstart));
                $item->event->K2BeforeDisplay = trim(implode("\n", $results));

                $results = $dispatcher->trigger('onK2AfterDisplay', array(&$item, &$params, $limitstart));
                $item->event->K2AfterDisplay = trim(implode("\n", $results));

                $results = $dispatcher->trigger('onK2AfterDisplayTitle', array(&$item, &$params, $limitstart));
                $item->event->K2AfterDisplayTitle = trim(implode("\n", $results));

                $results = $dispatcher->trigger('onK2BeforeDisplayContent', array(&$item, &$params, $limitstart));
                $item->event->K2BeforeDisplayContent = trim(implode("\n", $results));

                $results = $dispatcher->trigger('onK2AfterDisplayContent', array(&$item, &$params, $limitstart));
                $item->event->K2AfterDisplayContent = trim(implode("\n", $results));

                $dispatcher->trigger('onK2PrepareContent', array(&$item, &$params, $limitstart));

                if ($params->get('bnrItemCommentsCounter')) {
                    $results = $dispatcher->trigger('onK2CommentsCounter', array(&$item, &$params, $limitstart));
                    $item->event->K2CommentsCounter = trim(implode("\n", $results));
                }

            }

        }

        // Restore the intotext variable after plugins execution
        $item->introtext = $item->text;

        //Clean the plugin tags
        $item->introtext = preg_replace("#{(.*?)}(.*?){/(.*?)}#s", '', $item->introtext);

        //Author
        if ($params->get('bnrItemAuthor')) {
            if (!empty($item->created_by_alias)) {
                $item->author = $item->created_by_alias;
                $item->authorGender = NULL;
                $item->authorDescription = NULL;
                if ($params->get('itemAuthorAvatar'))
                    $item->authorAvatar = K2HelperUtilities::getAvatar('alias');
                $item->authorLink = Juri::root(true);
            } else {
                $author = JFactory::getUser($item->created_by);
                $item->author = $author->name;

                $query = "SELECT `description`, `gender` FROM #__k2_users WHERE userID=" . (int)$author->id;
                $db->setQuery($query, 0, 1);
                $result = $db->loadObject();
                if ($result) {
                    $item->authorGender = $result->gender;
                    $item->authorDescription = $result->description;
                } else {
                    $item->authorGender = NULL;
                    $item->authorDescription = NULL;
                }
                if ($params->get('bnrItemAuthorAvatar')) {
                    $item->authorAvatar = K2HelperUtilities::getAvatar($author->id, $author->email, $componentParams->get('userImageWidth'));
                }
                //Author Link
                $item->authorLink = JRoute::_(K2HelperRoute::getUserRoute($item->created_by));
            }
        }

        if (is_array($item->extra_fields)) {
            foreach ($item->extra_fields as $key => $extraField) {
                if ($extraField->type == 'textarea' || $extraField->type == 'textfield') {
                    $tmp = new JObject();
                    $tmp->text = $extraField->value;
                    if ($params->get('JPlugins', 1)) {
                        if (K2_JVERSION != '15') {
                            $dispatcher->trigger('onContentPrepare', array('mod_k2_content', &$tmp, &$params, $limitstart));
                        } else {
                            $dispatcher->trigger('onPrepareContent', array(&$tmp, &$params, $limitstart));
                        }
                    }
                    if ($params->get('K2Plugins', 1)) {
                        $dispatcher->trigger('onK2PrepareContent', array(&$tmp, &$params, $limitstart));
                    }
                    $extraField->value = $tmp->text;
                }
            }
        }

        return $item;


    }
}
