(function ($) {
    $(document).ready(function () {
        var $Map = $('.Map');

        if ($Map.length) {
            var Cover_Class = 'Map-Cover';

            var Cover_Object = $('<div/>', {
                'class': 'Map-Cover',
                'click': function () {
                    $(this).hide();
                }
            }).css({
                'position': 'absolute',
                'width': '100%',
                'height': '100%',
                'z-index': '1'
            });

            $Map.css('position', 'relative').append(Cover_Object).mouseleave(function () {
                $('.' + Cover_Class, this).show();
            });
        }
    });

})(jQuery);