(function ($) {
    $(document).ready(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Map Cover
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $('body').on('click', '.Map-Cover', function (e) {
            $(this).hide();
        }).on('mouseleave', '.Map', function () {
            $('.Map-Cover', this).show();
        });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Mobile Menu
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $('.Mobile-Menu').append($('.Menu').clone());

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Active Item
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $('a').each(function () {
            if ($(this).attr('href') == location.pathname) {
                $(this).addClass('Active');
            }
        });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Align Text
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $('.Letters>p').each(function () {
            $(this).html(Wrap($(this)));
        });

        function Wrap(target) {
            var newtarget = $("<div></div>");
            nodes = target.contents().clone();
            nodes.each(function () {
                if (this.nodeType == 3) { // Text
                    var newhtml = "";
                    var text = this.wholeText;
                    for (var i = 0; i < text.length; i++) {
                        if (text[i] == ' ') {
                            newhtml += " ";
                        }
                        else {
                            newhtml += "<small>" + text[i] + "</small>";
                        }
                    }
                    newtarget.append($(newhtml));
                }
                else {
                    $(this).html(Wrap($(this)));
                    newtarget.append($(this));
                }
            });
            return newtarget.html();
        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Projects Align
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        var Projects_Count = $('.Project').length;

        if ($('.Project').length == 9 && $('body').hasClass('Page-Projects')) {
            $($('.Project')[6]).after('<a class="Project Empty-Project"></a>');
        }


        if ($('body').hasClass('Page-Home')) {
            $($('.Project')[6]).after('<a class="Project Empty-Project"></a>');
        }


        if (!$('body').hasClass('Page-Pr') && !$('body').hasClass('Page-Gr')) {
            $('.Project').each(function ($Index, $Item) {
                // Desktop

                if ($Index % 9 == 0 || $Index % 10 == 0) {
                    $(this).addClass('Big-Desktop');
                }

                if (!$('body').hasClass('Page-Home')) {
                    if ($Index % 7 == 0) {
                        $(this).addClass('Desktop-Clear-Left');
                    }
                }

                // Tab

                if ($Index % 5 == 0 || $Index % 6 == 0) {
                    $(this).addClass('Big-Tab');
                }


                // Mobile
                if ($Index % 5 == 0) {
                    $(this).addClass('Big-Mobile');
                }


                // No-Image
                if (!$('.Project-Image img', this).length) {
                    $(this).addClass('No-Image');
                }

                if (!--Projects_Count) {
                    $('.Project').each(function () {
                        $(this).css('height', $(this).actual('outerWidth'));
                    });

                    // Delimiter
                    $('.Project').each(function ($Index, $Item) {
                        if ($Index % 5 == 0 && $Index != 0) {
                            $(this).before('<hr>');
                        }
                    });
                }
            });
        }

        // Set Height
        $('.Project').each(function () {
            $(this).css('height', $(this).outerWidth());
        });

        $(window).resize(function () {
            $('.Project').each(function () {
                $(this).css('height', $(this).outerWidth());
            });
        });


        if ($('body').hasClass('Page-Home') || $('body').hasClass('Page-Projects')) {
            if ($(window).width() >= 743 && $(window).width() < 983) {
                var Current = $('.Project.Introtext');
                $('.Project:nth-of-type(2)').insertAfter(Current);
            }

            $(window).resize(function () {
                var Current = $('.Project.Introtext');
                if ($(window).width() >= 723 && $(window).width() < 983) {
                    $('.Project:nth-of-type(1)').after(Current);
                } else {
                    $('.Project:nth-of-type(3)').after(Current);
                }
            });
        }


        // Header Copy
        if ($('body').hasClass('Page-Projects') && $('body').hasClass('Page-Level-2')) {
            $('.Project-Left-Row').before($('.Project-Right-Row h2.Title').clone().addClass('Title-Mobile').removeClass('Title'));
        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Map Init
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function Map_Init() {
            var mapOptions = {
                zoom: 17,
                center: new google.maps.LatLng(55.729551, 37.664163),
                styles: [{
                    "featureType": "landscape",
                    "stylers": [{"saturation": -100}, {"lightness": 65}, {"visibility": "on"}]
                }, {
                    "featureType": "poi",
                    "stylers": [{"saturation": -100}, {"lightness": 51}, {"visibility": "simplified"}]
                }, {
                    "featureType": "road.highway",
                    "stylers": [{"saturation": -100}, {"visibility": "simplified"}]
                }, {
                    "featureType": "road.arterial",
                    "stylers": [{"saturation": -100}, {"lightness": 30}, {"visibility": "on"}]
                }, {
                    "featureType": "road.local",
                    "stylers": [{"saturation": -100}, {"lightness": 40}, {"visibility": "on"}]
                }, {
                    "featureType": "transit",
                    "stylers": [{"saturation": -100}, {"visibility": "simplified"}]
                }, {
                    "featureType": "administrative.province",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "water",
                    "elementType": "labels",
                    "stylers": [{"visibility": "on"}, {"lightness": -25}, {"saturation": -100}]
                }, {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{"hue": "#ffff00"}, {"lightness": -25}, {"saturation": -97}]
                }]
            };
            var mapElement = $('.Map')[0];
            var map = new google.maps.Map(mapElement, mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(55.729551, 37.664163),
                map: map,
                title: 'Snazzy!'
            });


            marker.setIcon('/templates/osstro/img/ico-d-map-mark.png');


        }

        if ($('.Map').length) {
            Map_Init();
        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Map Init
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if ($('body').hasClass('Page-Blog') && $('body').hasClass('Page-Level-1') && $('.pagination-next').length) {
            $('.Pagination').prepend(
                $('.pagination-next a').clone().addClass('Next-Page').removeClass('hasTooltip pagenav').text('Показать еще 10 постов')
            );
        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Level 2 Links
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if ($('body').hasClass('Page-Level-2')) {
            if ($('body').hasClass('Page-Projects')) {
                $('.Menu .Current a').attr('href', '/projects');
            } else if ($('body').hasClass('Page-Blog')) {
                $('.Menu .Current a').attr('href', '/blog');
            }

            $('.Menu .Current').addClass('Active-But-Link');
        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Project Slider Item Hide
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if ($('body').hasClass('Page-Projects') && $('body').hasClass('Page-Projects')) {
            $('.Gallery').click(function () {
                $('.Gallery-Letters').fadeOut();
                $('.Grayscale').removeClass('Grayscale');
            });
        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Project Slider Item Hide
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if ($('body').hasClass('Page-Projects') && $('body').hasClass('Page-Projects')) {
            $('.Gallery').click(function () {
                $('.Gallery-Letters').fadeOut();
                $('.Grayscale').removeClass('Grayscale');
            });
        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Blog Same Posts
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if ($('body').hasClass('Page-Blog Page-Level-2') && !$('.Blog-Same-Post').length) {
            $('.After-Component').remove();
        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajax Form
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $('form .Send').click(function () {
            var Data = {};
            Data.name = $('.Modal input[name=name]').val();
            Data.phone = $('input[name=phone]').val();
            Data.message = $('textarea[name=message]').val();
            var $Message = $('.Message');

            if (Data.phone && Data.phone.length) {
                $.ajax({
                    'type': 'POST',
                    'url': '/libraries/message.php',
                    'data': Data,
                    'success': function () {
                        $Message.html('Спасибо за заявку!<br> С Вами свяжутся в ближайшее время!').addClass('Success').removeClass('Error');
                        $('.Modal form').remove();
                        $('.Modal h3').text('Имя и контакты отправлены');
                        $('.Modal').append('<br><p>Мы свяжемся с вами в скором времени.</p><button class="Close-Button">закрыть окно</button>');

                        //$('.Modal-Outer').fadeIn().delay(5000).fadeOut();

                        $('.Close-Button').click(function () {
                            $('.Modal-Outer').fadeOut();
                            return false;
                        });

                    },
                    'error': function () {
                        $Message.html('Ошибка отправки сообщения! Пропробуйте позже!').addClass('Error').removeClass('Success');
                    }
                });
            } else {
                $Message.html('Ошибка! Введите Ваш номер телефона!').addClass('Error').removeClass('Success');
            }

            $Message.fadeIn().delay(3000).fadeOut();

            return false;
        });


        $('.share').ShareLink({
            title: $('title').text(),
            text: $('title').text(),
            url: location.href
        });
        $('.counter').ShareCounter({
            url: location.href
        });


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    });
})(jQuery);