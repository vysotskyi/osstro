﻿<?php defined('_JEXEC') or die('Restricted access');

$App = JFactory::getApplication();
$Doc = JFactory::getDocument();
$User = JFactory::getUser();

$URI = array_values(array_filter(explode('?', $_SERVER['REQUEST_URI'])));
$URI = array_values(array_filter(explode('/', $URI[0])));
?>
<!-- OSSTRO -->

<!DOCTYPE html>

<html lang="<?php echo $Doc->language; ?>">
<head>
    <jdoc:include type="head"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="/templates/osstro/css/init.css">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="/templates/osstro/js/libs/fotorama.js"></script>
    <link rel="stylesheet" href="/templates/osstro/js/libs/fotorama.css">

    <link rel="apple-touch-icon" sizes="57x57" href="/templates/osstro/img/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/templates/osstro/img/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/templates/osstro/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/templates/osstro/img/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/templates/osstro/img/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/templates/osstro/img/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/templates/osstro/img/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/templates/osstro/img/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/templates/osstro/img/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/templates/osstro/img/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/templates/osstro/img/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/templates/osstro/img/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/templates/osstro/img/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/templates/osstro/img/manifest.json">
    <link rel="mask-icon" href="/templates/osstro/img/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#b91d47">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <script src="/templates/osstro/js/libs/jquery.actual.min.js"></script>
    <script src="/templates/osstro/js/libs/social.min.js"></script>
    <script src="/templates/osstro/js/index.js"></script>
</head>

<body
    class="<?= isset($URI[0]) ? 'Page-' . ucfirst($URI[0]) : 'Page-Home'; ?> <?= isset($URI[1]) ? 'Page-Level-2' : 'Page-Level-1'; ?> Script-Typography">
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '124782304556882',
            xfbml      : true,
            version    : 'v2.5'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<div class="Preloader"></div>
<div class="Modal-Outer">
    <div class="Modal">
        <a href="#" class="Close"></a>

        <h3>Начать диалог</h3>

        <form action="">
            <div class="Form-Row">
                <label for="User-Name">Имя</label>
                <input id="User-Name" name="name" type="text">
            </div>
            <div class="Form-Row">
                <label for="User-Contacts">Контакты</label>
                <input id="User-Contacts" name="phone" type="text">
            </div>
            <div class="Form-Row">
                <label for="User-Message">Сообщение</label>
                <textarea id="User-Message" name="message"></textarea>
            </div>
            <button class="Send">Отправить</button>
            <div class="Message"></div>
        </form>
    </div>
</div>


<main>
    <!-- START: Header -->
    <?php if ($this->countModules('Menu')) : ?>
        <header>
            <nav role="navigation">
                <div class="Mobile-Menu"></div>
                <a class="Company-Logo" href="/" title="На главную страницу"></a>
                <a href="#" class="Hamburger"><span></span></a>
                <jdoc:include type="modules" name="Menu"/>
            </nav>
        </header>
    <?php endif; ?>
    <!-- END: Header -->


    <!-- START: Before Component -->
    <?php if ($this->countModules('Before-Component')) : ?>
        <section class="Before-Component">
            <jdoc:include type="modules" name="Before-Component"/>
        </section>
    <?php endif; ?>
    <!-- END: Before Component -->


    <!-- START: Component -->
    <section class="Component">
        <jdoc:include type="component"/>
    </section>
    <!-- END: Component -->


    <!-- START: After Component -->
    <?php if ($this->countModules('After-Component')) : ?>
        <section class="After-Component">
            <jdoc:include type="modules" name="After-Component"/>

            <?php if ($this->countModules('Same-Posts') && isset($URI[0]) && $URI[0] == 'blog' && isset($URI[1])) { ?>
                <jdoc:include type="modules" name="Same-Posts"/>
            <?php } ?>
        </section>
    <?php endif; ?>
    <!-- END: After Component -->

</main>


<!-- START: Footer -->
<?php if ($this->countModules('Footer')) : ?>
    <footer class="Footer-Inner">
        <jdoc:include type="modules" name="Footer"/>
    </footer>
<?php endif; ?>
<!-- END: Footer -->

<script src="/templates/osstro/js/plugins/jquery.descroll-map.js"></script>
<script src="/templates/osstro/js/plugins/jquery.modal.js"></script>
<script src="/templates/osstro/js/plugins/jquery.mobile-menu.js"></script>
<script src="/templates/osstro/js/plugins/data.vertical-align.js"></script>
<script src="/templates/osstro/js/plugins/data.typography.js"></script>

</body>
</html>


