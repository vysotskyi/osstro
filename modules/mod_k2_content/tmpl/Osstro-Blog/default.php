<?php defined('_JEXEC') or die; ?>

<div class="Blog-Same-Posts">

    <?php foreach ($items as $key => $item): ?>


        <a class="Blog-Same-Post" href="<?php echo $item->link; ?>">
            <header>
                <h2 class="Blog-Post-Title">
                    <?php echo $item->title; ?>
                </h2>

                <p class="Blog-Post-Date">
                    <?= str_replace(date("Y"),'',JHTML::_('date', $item->created, 'j F Y')) ?>
                </p>
            </header>


            <div class="Blog-Same-Post-Image">
                <?php if (!empty($item->image)) { ?>
                    <img src="<?php echo $item->image; ?>">
                <?php } ?>
            </div>
        </a>

    <?php endforeach; ?>
</div>