<?php
/**
 * @version        2.6.x
 * @package        K2
 * @author        JoomlaWorks http://www.joomlaworks.net
 * @copyright    Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license        GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Set Extra To Names
$Extra_Fields = [];
foreach ($this->item->extra_fields as $Key => $Value) {
    $Extra_Fields[$Value->alias] = $Value->value;
}

?>




<?php if (JRequest::getInt('print') == 1): ?>
    <!-- Print button at the top of the print page only -->
    <a class="itemPrintThisPage" rel="nofollow" href="#" onclick="window.print();return false;">
        <span><?php echo JText::_('K2_PRINT_THIS_PAGE'); ?></span>
    </a>
<?php endif; ?>

<!-- Start K2 Item Layout -->
<span id="startOfPageId<?php echo JRequest::getInt('id'); ?>"></span>

<div class="Project-Item<?=!count($this->item->attachments)?' No-Gallery':''?>">

    <?php if ($this->item->params->get('itemAttachments') && count($this->item->attachments)): ?>
        <div class="Gallery Grayscale">
            <?php if (isset($Extra_Fields['Letters'])) { ?>
                <div class="Gallery-Letters Script-Vertical-Align">
                    <?php echo $Extra_Fields['Letters']; ?>
                </div>
            <?php } ?>
            <ul class="fotorama" data-loop="true" data-width="100%" data-ratio="1920/1080" >
                <?php foreach ($this->item->attachments as $attachment): ?>
                    <li><img src="<?php echo $attachment->link; ?>" alt=""></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <div class="Project-Left-Row">
        <p>
            <?php if (isset($Extra_Fields['Years'])) { ?><span
                class="Year"><?php echo $Extra_Fields['Years']; ?></span><?php } ?>

            <?php if (isset($Extra_Fields['Organization'])) { ?><span
                class="Organization"><?php echo $Extra_Fields['Organization']; ?></span><?php } ?>
        </p>

        <?php if (isset($Extra_Fields['Services'])) { ?>
            <div class="Services">
                <?php echo $Extra_Fields['Services']; ?>
            </div>
        <?php } ?>

    </div>

    <div class="Project-Right-Row">
        <h2 class="Title">
            <?php echo $this->item->title; ?>
        </h2>

        <div class="Description">
            <?php echo $this->item->introtext; ?>
            <?php echo $this->item->fulltext; ?>
        </div>


        <?php
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Load Comments
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if (isset($Extra_Fields['related_comments'])) {
            $DB = JFactory::getDbo();
            $Query = $DB->getQuery(true);

            $Query->select('*');
            $Query->from($DB->quoteName('#__k2_items'));
            $Query->where($DB->quoteName('catid') . ' = 4');
            $Query->where($DB->quoteName('id') . ' IN (' . $Extra_Fields['related_comments'] . ')');
            $Query->order('id ASC');


            $DB->setQuery($Query);
            $Comments_List = $DB->loadObjectList();


            echo '<section class="Comments">';

            foreach ($Comments_List as $Key => $Comment) {
                $Comments_Extra_Fields = json_decode($Comment->extra_fields);
                $Image = JURI::base(true) . '/media/k2/items/src/' . md5("Image" . $Comment->id) . '.jpg';
                ?>

                <article class="Comment">
                    <div class="Comment-Image">
                        <img src="<?php echo $Image; ?>">
                    </div>
                    <div class="Comment-Body">
                        <h4 class="Comment-Author">
                            <?php echo $Comment->title; ?>
                        </h4>
                        <?php if (!empty($Comments_Extra_Fields[0]->value)) { ?>
                            <p class="Comment-Position">
                                <?php echo $Comments_Extra_Fields[0]->value; ?>
                            </p>
                        <?php } ?>

                        <div class="Comment-Content">
                            <?php echo $Comment->introtext; ?>
                        </div>
                    </div>
                </article>

                <?php

            }
            echo '</section>';
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ?>
    </div>


    <!-- Plugins: BeforeDisplay -->
    <?php echo $this->item->event->BeforeDisplay; ?>

    <!-- K2 Plugins: K2BeforeDisplay -->
    <?php echo $this->item->event->K2BeforeDisplay; ?>


</div>

<!-- Plugins: AfterDisplayTitle -->
<?php echo $this->item->event->AfterDisplayTitle; ?>

<!-- K2 Plugins: K2AfterDisplayTitle -->
<?php echo $this->item->event->K2AfterDisplayTitle; ?>


<div class="itemBody">

    <!-- Plugins: BeforeDisplayContent -->
    <?php echo $this->item->event->BeforeDisplayContent; ?>

    <!-- K2 Plugins: K2BeforeDisplayContent -->
    <?php echo $this->item->event->K2BeforeDisplayContent; ?>

    <?php if ($this->item->params->get('itemImage') && !empty($this->item->image)): ?>
        <!-- Item Image -->
        <div class="itemImageBlock">
		  <span class="itemImage">
		  	<a class="modal" rel="{handler: 'image'}" href="<?php echo $this->item->imageXLarge; ?>"
               title="<?php echo JText::_('K2_CLICK_TO_PREVIEW_IMAGE'); ?>">
                <img src="<?php echo $this->item->image; ?>"
                     alt="<?php if (!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>"
                     style="width:<?php echo $this->item->imageWidth; ?>px; height:auto;"/>
            </a>
		  </span>

            <?php if ($this->item->params->get('itemImageMainCaption') && !empty($this->item->image_caption)): ?>
                <!-- Image caption -->
                <span class="itemImageCaption"><?php echo $this->item->image_caption; ?></span>
            <?php endif; ?>

            <?php if ($this->item->params->get('itemImageMainCredits') && !empty($this->item->image_credits)): ?>
                <!-- Image credits -->
                <span class="itemImageCredits"><?php echo $this->item->image_credits; ?></span>
            <?php endif; ?>

            <div class="clr"></div>
        </div>
    <?php endif; ?>

    <?php if ($this->item->params->get('itemHits') || ($this->item->params->get('itemDateModified') && intval($this->item->modified) != 0)): ?>
        <div class="itemContentFooter">

            <?php if ($this->item->params->get('itemHits')): ?>
                <!-- Item Hits -->
                <span class="itemHits">
				<?php echo JText::_('K2_READ'); ?>
                    <b><?php echo $this->item->hits; ?></b> <?php echo JText::_('K2_TIMES'); ?>
			</span>
            <?php endif; ?>

            <?php if ($this->item->params->get('itemDateModified') && intval($this->item->modified) != 0): ?>
                <!-- Item date modified -->
                <span class="itemDateModified">
				<?php echo JText::_('K2_LAST_MODIFIED_ON'); ?><?php echo JHTML::_('date', $this->item->modified, JText::_('K2_DATE_FORMAT_LC2')); ?>
			</span>
            <?php endif; ?>

            <div class="clr"></div>
        </div>
    <?php endif; ?>

    <!-- Plugins: AfterDisplayContent -->
    <?php echo $this->item->event->AfterDisplayContent; ?>

    <!-- K2 Plugins: K2AfterDisplayContent -->
    <?php echo $this->item->event->K2AfterDisplayContent; ?>

    <div class="clr"></div>
</div>

<?php if ($this->item->params->get('itemTwitterButton', 1) || $this->item->params->get('itemFacebookButton', 1) || $this->item->params->get('itemGooglePlusOneButton', 1)): ?>
    <!-- Social sharing -->
    <div class="itemSocialSharing">

        <?php if ($this->item->params->get('itemTwitterButton', 1)): ?>
            <!-- Twitter Button -->
            <div class="itemTwitterButton">
                <a href="https://twitter.com/share" class="twitter-share-button"
                   data-count="horizontal"<?php if ($this->item->params->get('twitterUsername')): ?> data-via="<?php echo $this->item->params->get('twitterUsername'); ?>"<?php endif; ?>>
                    <?php echo JText::_('K2_TWEET'); ?>
                </a>
                <script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
            </div>
        <?php endif; ?>

        <?php if ($this->item->params->get('itemFacebookButton', 1)): ?>
            <div class="Project-Social">
                <div class="Share-Links">
                    <div class='like-block Share-Facebook like-button share s_facebook'>
                        <span class='like-button share s_facebook'></span>
                        <span class='like-counter counter c_facebook'></span>
                    </div>

                    <div class='like-block Share-Twitter like-button share s_twitter'>
                        <span class='like-button share s_twitter'></span>
                        <span class='like-counter counter c_twitter'></span>
                    </div>

                    <div class='like-block Share-Vk like-button share s_vk'>
                        <span class='like-button share s_vk'></span>
                        <span class='like-counter counter c_vk'></span>
                    </div>
                </div>

                <!-- Facebook Button -->
                <div class="Like-Facebook   " id="fb-root"></div>
                <script>(function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "//connect.facebook.net/en_EN/sdk.js#xfbml=1&version=v2.5&appId=335556789884889";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

                <div class="fb-like" data-href="<?php echo $this->item->link; ?>" data-width="40px"
                     data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                <!-- Facebook Button -->

            </div>
        <?php endif; ?>

        <?php if ($this->item->params->get('itemGooglePlusOneButton', 1)): ?>
            <!-- Google +1 Button -->
            <div class="itemGooglePlusOneButton">
                <g:plusone annotation="inline" width="120"></g:plusone>
                <script type="text/javascript">
                    (function () {
                        window.___gcfg = {lang: 'en'}; // Define button default language here
                        var po = document.createElement('script');
                        po.type = 'text/javascript';
                        po.async = true;
                        po.src = 'https://apis.google.com/js/plusone.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(po, s);
                    })();
                </script>
            </div>
        <?php endif; ?>

        <div class="clr"></div>
    </div>
<?php endif; ?>

<?php if ($this->item->params->get('itemCategory') || $this->item->params->get('itemTags') || $this->item->params->get('itemAttachments')): ?>
    <div class="itemLinks">

        <?php if ($this->item->params->get('itemCategory')): ?>
            <!-- Item category -->
            <div class="itemCategory">
                <span><?php echo JText::_('K2_PUBLISHED_IN'); ?></span>
                <a href="<?php echo $this->item->category->link; ?>"><?php echo $this->item->category->name; ?></a>
            </div>
        <?php endif; ?>

        <?php if ($this->item->params->get('itemTags') && count($this->item->tags)): ?>
            <!-- Item tags -->
            <div class="itemTagsBlock">
                <span><?php echo JText::_('K2_TAGGED_UNDER'); ?></span>
                <ul class="itemTags">
                    <?php foreach ($this->item->tags as $tag): ?>
                        <li><a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <div class="clr"></div>
            </div>
        <?php endif; ?>


    </div>
<?php endif; ?>

<?php if ($this->item->params->get('itemAuthorBlock') && empty($this->item->created_by_alias)): ?>
    <!-- Author Block -->
    <div class="itemAuthorBlock">

        <?php if ($this->item->params->get('itemAuthorImage') && !empty($this->item->author->avatar)): ?>
            <img class="itemAuthorAvatar" src="<?php echo $this->item->author->avatar; ?>"
                 alt="<?php echo K2HelperUtilities::cleanHtml($this->item->author->name); ?>"/>
        <?php endif; ?>

        <div class="itemAuthorDetails">
            <h3 class="itemAuthorName">
                <a rel="author"
                   href="<?php echo $this->item->author->link; ?>"><?php echo $this->item->author->name; ?></a>
            </h3>

            <?php if ($this->item->params->get('itemAuthorDescription') && !empty($this->item->author->profile->description)): ?>
                <p><?php echo $this->item->author->profile->description; ?></p>
            <?php endif; ?>

            <?php if ($this->item->params->get('itemAuthorURL') && !empty($this->item->author->profile->url)): ?>
                <span class="itemAuthorUrl"><?php echo JText::_('K2_WEBSITE'); ?> <a rel="me"
                                                                                     href="<?php echo $this->item->author->profile->url; ?>"
                                                                                     target="_blank"><?php echo str_replace('http://', '', $this->item->author->profile->url); ?></a></span>
            <?php endif; ?>

            <?php if ($this->item->params->get('itemAuthorEmail')): ?>
                <span
                    class="itemAuthorEmail"><?php echo JText::_('K2_EMAIL'); ?><?php echo JHTML::_('Email.cloak', $this->item->author->email); ?></span>
            <?php endif; ?>

            <div class="clr"></div>

            <!-- K2 Plugins: K2UserDisplay -->
            <?php echo $this->item->event->K2UserDisplay; ?>

        </div>
        <div class="clr"></div>
    </div>
<?php endif; ?>

<?php if ($this->item->params->get('itemAuthorLatest') && empty($this->item->created_by_alias) && isset($this->authorLatestItems)): ?>
    <!-- Latest items from author -->
    <div class="itemAuthorLatest">
        <h3><?php echo JText::_('K2_LATEST_FROM'); ?><?php echo $this->item->author->name; ?></h3>
        <ul>
            <?php foreach ($this->authorLatestItems as $key => $item): ?>
                <li class="<?php echo ($key % 2) ? "odd" : "even"; ?>">
                    <a href="<?php echo $item->link ?>"><?php echo $item->title; ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="clr"></div>
    </div>
<?php endif; ?>

<?php
/*
Note regarding 'Related Items'!
If you add:
- the CSS rule 'overflow-x:scroll;' in the element div.itemRelated {…} in the k2.css
- the class 'k2Scroller' to the ul element below
- the classes 'k2ScrollerElement' and 'k2EqualHeights' to the li element inside the foreach loop below
- the style attribute 'style="width:<?php echo $item->imageWidth; ?>px;"' to the li element inside the foreach loop below
...then your Related Items will be transformed into a vertical-scrolling block, inside which, all items have the same height (equal column heights). This can be very useful if you want to show your related articles or products with title/author/category/image etc., which would take a significant amount of space in the classic list-style display.
*/
?>


<div class="clr"></div>

<?php if ($this->item->params->get('itemVideo') && !empty($this->item->video)): ?>
    <!-- Item video -->
    <a name="itemVideoAnchor" id="itemVideoAnchor"></a>

    <div class="itemVideoBlock">
        <h3><?php echo JText::_('K2_MEDIA'); ?></h3>

        <?php if ($this->item->videoType == 'embedded'): ?>
            <div class="itemVideoEmbedded">
                <?php echo $this->item->video; ?>
            </div>
        <?php else: ?>
            <span class="itemVideo"><?php echo $this->item->video; ?></span>
        <?php endif; ?>

        <?php if ($this->item->params->get('itemVideoCaption') && !empty($this->item->video_caption)): ?>
            <span class="itemVideoCaption"><?php echo $this->item->video_caption; ?></span>
        <?php endif; ?>

        <?php if ($this->item->params->get('itemVideoCredits') && !empty($this->item->video_credits)): ?>
            <span class="itemVideoCredits"><?php echo $this->item->video_credits; ?></span>
        <?php endif; ?>

        <div class="clr"></div>
    </div>
<?php endif; ?>

<?php if ($this->item->params->get('itemImageGallery') && !empty($this->item->gallery)): ?>
    <!-- Item image gallery -->
    <a name="itemImageGalleryAnchor" id="itemImageGalleryAnchor"></a>
    <div class="itemImageGallery">
        <h3><?php echo JText::_('K2_IMAGE_GALLERY'); ?></h3>
        <?php echo $this->item->gallery; ?>
    </div>
<?php endif; ?>

<?php if ($this->item->params->get('itemNavigation') && !JRequest::getCmd('print') && (isset($this->item->nextLink) || isset($this->item->previousLink))): ?>
    <!-- Item navigation -->
    <div class="itemNavigation">
        <span class="itemNavigationTitle"><?php echo JText::_('K2_MORE_IN_THIS_CATEGORY'); ?></span>

        <?php if (isset($this->item->previousLink)): ?>
            <a class="itemPrevious" href="<?php echo $this->item->previousLink; ?>">
                &laquo; <?php echo $this->item->previousTitle; ?>
            </a>
        <?php endif; ?>

        <?php if (isset($this->item->nextLink)): ?>
            <a class="itemNext" href="<?php echo $this->item->nextLink; ?>">
                <?php echo $this->item->nextTitle; ?> &raquo;
            </a>
        <?php endif; ?>

    </div>
<?php endif; ?>

<!-- Plugins: AfterDisplay -->
<?php echo $this->item->event->AfterDisplay; ?>

<!-- K2 Plugins: K2AfterDisplay -->
<?php echo $this->item->event->K2AfterDisplay; ?>

<?php if ($this->item->params->get('itemComments') && (($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1'))): ?>
    <!-- K2 Plugins: K2CommentsBlock -->
    <?php echo $this->item->event->K2CommentsBlock; ?>
<?php endif; ?>

<?php if ($this->item->params->get('itemComments') && ($this->item->params->get('comments') == '1' || ($this->item->params->get('comments') == '2')) && empty($this->item->event->K2CommentsBlock)): ?>
    <!-- Item comments -->
    <a name="itemCommentsAnchor" id="itemCommentsAnchor"></a>

    <div class="itemComments">

        <?php if ($this->item->params->get('commentsFormPosition') == 'above' && $this->item->params->get('itemComments') && !JRequest::getInt('print') && ($this->item->params->get('comments') == '1' || ($this->item->params->get('comments') == '2' && K2HelperPermissions::canAddComment($this->item->catid)))): ?>
            <!-- Item comments form -->
            <div class="itemCommentsForm">
                <?php echo $this->loadTemplate('comments_form'); ?>
            </div>
        <?php endif; ?>

        <?php if ($this->item->numOfComments > 0 && $this->item->params->get('itemComments') && ($this->item->params->get('comments') == '1' || ($this->item->params->get('comments') == '2'))): ?>
            <!-- Item user comments -->
            <h3 class="itemCommentsCounter">
                <span><?php echo $this->item->numOfComments; ?></span> <?php echo ($this->item->numOfComments > 1) ? JText::_('K2_COMMENTS') : JText::_('K2_COMMENT'); ?>
            </h3>

            <ul class="itemCommentsList">
                <?php foreach ($this->item->comments as $key => $comment): ?>
                    <li class="<?php echo ($key % 2) ? "odd" : "even";
                    echo (!$this->item->created_by_alias && $comment->userID == $this->item->created_by) ? " authorResponse" : "";
                    echo ($comment->published) ? '' : ' unpublishedComment'; ?>">

	    	<span class="commentLink">
		    	<a href="<?php echo $this->item->link; ?>#comment<?php echo $comment->id; ?>"
                   name="comment<?php echo $comment->id; ?>" id="comment<?php echo $comment->id; ?>">
                    <?php echo JText::_('K2_COMMENT_LINK'); ?>
                </a>
		    </span>

                        <?php if ($comment->userImage): ?>
                            <img src="<?php echo $comment->userImage; ?>"
                                 alt="<?php echo JFilterOutput::cleanText($comment->userName); ?>"
                                 width="<?php echo $this->item->params->get('commenterImgWidth'); ?>"/>
                        <?php endif; ?>

                        <span class="commentDate">
		    	<?php echo JHTML::_('date', $comment->commentDate, JText::_('K2_DATE_FORMAT_LC2')); ?>
		    </span>

		    <span class="commentAuthorName">
			    <?php echo JText::_('K2_POSTED_BY'); ?>
                <?php if (!empty($comment->userLink)): ?>
                    <a href="<?php echo JFilterOutput::cleanText($comment->userLink); ?>"
                       title="<?php echo JFilterOutput::cleanText($comment->userName); ?>" target="_blank"
                       rel="nofollow">
                        <?php echo $comment->userName; ?>
                    </a>
                <?php else: ?>
                    <?php echo $comment->userName; ?>
                <?php endif; ?>
		    </span>

                        <p><?php echo $comment->commentText; ?></p>

                        <?php if ($this->inlineCommentsModeration || ($comment->published && ($this->params->get('commentsReporting') == '1' || ($this->params->get('commentsReporting') == '2' && !$this->user->guest)))): ?>
                            <span class="commentToolbar">
					<?php if ($this->inlineCommentsModeration): ?>
                        <?php if (!$comment->published): ?>
                            <a class="commentApproveLink"
                               href="<?php echo JRoute::_('index.php?option=com_k2&view=comments&task=publish&commentID=' . $comment->id . '&format=raw') ?>"><?php echo JText::_('K2_APPROVE') ?></a>
                        <?php endif; ?>

					<a class="commentRemoveLink"
                       href="<?php echo JRoute::_('index.php?option=com_k2&view=comments&task=remove&commentID=' . $comment->id . '&format=raw') ?>"><?php echo JText::_('K2_REMOVE') ?></a>
                    <?php endif; ?>

                                <?php if ($comment->published && ($this->params->get('commentsReporting') == '1' || ($this->params->get('commentsReporting') == '2' && !$this->user->guest))): ?>
                                    <a class="modal" rel="{handler:'iframe',size:{x:560,y:480}}"
                                       href="<?php echo JRoute::_('index.php?option=com_k2&view=comments&task=report&commentID=' . $comment->id) ?>"><?php echo JText::_('K2_REPORT') ?></a>
                                <?php endif; ?>

                                <?php if ($comment->reportUserLink): ?>
                                    <a class="k2ReportUserButton"
                                       href="<?php echo $comment->reportUserLink; ?>"><?php echo JText::_('K2_FLAG_AS_SPAMMER'); ?></a>
                                <?php endif; ?>

				</span>
                        <?php endif; ?>

                        <div class="clr"></div>
                    </li>
                <?php endforeach; ?>
            </ul>

            <div class="itemCommentsPagination">
                <?php echo $this->pagination->getPagesLinks(); ?>
                <div class="clr"></div>
            </div>
        <?php endif; ?>

        <?php if ($this->item->params->get('commentsFormPosition') == 'below' && $this->item->params->get('itemComments') && !JRequest::getInt('print') && ($this->item->params->get('comments') == '1' || ($this->item->params->get('comments') == '2' && K2HelperPermissions::canAddComment($this->item->catid)))): ?>
            <!-- Item comments form -->
            <div class="itemCommentsForm">
                <?php echo $this->loadTemplate('comments_form'); ?>
            </div>
        <?php endif; ?>

        <?php $user = JFactory::getUser();
        if ($this->item->params->get('comments') == '2' && $user->guest): ?>
            <div><?php echo JText::_('K2_LOGIN_TO_POST_COMMENTS'); ?></div>
        <?php endif; ?>

    </div>
<?php endif; ?>
<!-- End K2 Item Layout -->

<?php
// Render modules inside this article override
$doc = JFactory::getDocument();
$renderer = $doc->loadRenderer("modules");
$raw = ['style' => 'standard'];

// Echo the module somewhere in the template
echo $renderer->render("Project-Letters", $raw, null);
echo $renderer->render("Project-Component", $raw, null); ?>

