<?php
/**
 * @version        2.6.x
 * @package        K2
 * @author        JoomlaWorks http://www.joomlaworks.net
 * @copyright    Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license        GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);

// Set Extra To Names
$Extra_Fields = [];
foreach ($this->item->extra_fields as $Key => $Value) {
    $Extra_Fields[$Value->alias] = $Value->value;
}
?>

<article class="Comment">
    <div class="Comment-Body">
        <h4 class="Comment-Author">
            <?php echo $this->item->title; ?>
        </h4>
        <?php if (!empty($Extra_Fields['Position'])) { ?>
            <p class="Comment-Position">
                <?php echo $Extra_Fields['Position']; ?>
            </p>
        <?php } ?>

        <div class="Comment-Content">
            <?php echo $this->item->introtext; ?>
        </div>
    </div>
    <div class="Comment-Image">
        <img src="<?php echo $this->item->image; ?>">
    </div>
</article>
























