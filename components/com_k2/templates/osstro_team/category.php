<?php
/**
 * @version        2.6.x
 * @package        K2
 * @author        JoomlaWorks http://www.joomlaworks.net
 * @copyright    Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license        GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>


<section class="Team">
    <h2 class="Team-Header<?php echo $this->params->get('pageclass_sfx')?>">
        <?php echo $this->escape($this->params->get('page_title')); ?>
    </h2>
    <?php foreach ($this->leading as $key => $item): ?>
        <?php
        $this->item = $item;
        echo $this->loadTemplate('item');
        ?>
    <?php endforeach; ?>
</section>
