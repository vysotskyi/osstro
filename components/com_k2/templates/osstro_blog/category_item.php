<?php

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);

// Set Extra To Names
$Extra_Fields = [];
foreach ($this->item->extra_fields as $Key => $Value) {
    $Extra_Fields[$Value->alias] = $Value->value;
}

?>

<article class="Blog-Post">
    <?php if (!empty($this->item->image)) { ?>
        <div class="Blog-Post-Image">
            <img src="<?php echo $this->item->image; ?>">
        </div>
    <?php } ?>

    <a href="<?= $this->item->link; ?>"><h3 class="Blog-Post-Title"><span><?php echo $this->item->title; ?></span></h3>
    </a>

    <div class="Blog-Post-Introtext"><?php echo $this->item->introtext; ?><?php echo $this->item->fulltext; ?></div>

    <div class="Blog-Post-Meta">
        <span class="Blog-Post-Author"><?= $this->item->author->name; ?></span>
        <span class="Blog-Post-Tags">
            <?php foreach ($this->item->tags as $Tag_Index => $Tag) { ?><?= ($Tag_Index > 0) ? ', ' : '' ?><?= $Tag->name; ?><?php } ?>
        </span>

        <span
            class="Blog-Post-Date"><?= str_replace(date("Y"), '', JHTML::_('date', $this->item->created, 'j F Y')) ?></span>
    </div>

</article>
