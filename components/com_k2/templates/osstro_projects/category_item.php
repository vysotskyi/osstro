<?php
/**
 * @version        2.6.x
 * @package        K2
 * @author        JoomlaWorks http://www.joomlaworks.net
 * @copyright    Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license        GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);

// Set Extra To Names
$Extra_Fields = [];
if(isset($this->item->extra_fields)){
    foreach ($this->item->extra_fields as $Key => $Value) {
        $Extra_Fields[$Value->alias] = $Value->value;
    }
}

?>

<?php if ($this->item->params->get('catItemIntroText')) { ?>
    <a class="Project Introtext">
        <?php echo $this->item->introtext; ?>
    </a>
<?php } else { ?>

    <a class="Project" href="/projects<?php echo $this->item->link; ?>">
        <?php if ($this->item->params->get('catItemTitle')) { ?>
            <h3>
                <?php echo $this->item->title; ?>
            </h3>
        <?php } ?>

        <div class="Project-Image">
            <?php if ($this->item->params->get('catItemImage') && !empty($this->item->image)) { ?>
                <img src="<?php echo $this->item->image; ?>">
            <?php } ?>
        </div>
    </a>
<?php } ?>
