<?php
/**
 * @version        2.6.x
 * @package        K2
 * @author        JoomlaWorks http://www.joomlaworks.net
 * @copyright    Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license        GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>

<!-- Start K2 Category Layout -->
<div class="Projects">
    <div class="Projects-List">
        <?php foreach ($items as $key => $item): ?>

            <?php if (!json_decode($item->params)->catItemIntroText) { ?>

                <a class="Project" href="/projects<?php echo $item->link; ?>">
                    <h3>
                        <?php echo $item->title; ?>
                    </h3>

                    <div class="Project-Image">
                        <?php if (!empty($item->image)) { ?>
                            <img src="<?php echo $item->image; ?>">
                        <?php } ?>
                    </div>
                </a>
            <?php } ?>

        <?php endforeach; ?>
    </div>
</div>
<!-- End K2 Category Layout -->
