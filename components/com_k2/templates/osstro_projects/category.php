<?php
/**
 * @version        2.6.x
 * @package        K2
 * @author        JoomlaWorks http://www.joomlaworks.net
 * @copyright    Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license        GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

$URI = array_values(array_filter(explode('?', $_SERVER['REQUEST_URI'])));
$URI = array_values(array_filter(explode('/', $URI[0])));

if (!isset($URI[0])) {
    $Max_Items = 9;
} else {
    $Max_Items = 100;
}


?>


<!-- Start K2 Category Layout -->
<div
    class="Projects<?php if ($this->params->get('pageclass_sfx')) echo ' ' . $this->params->get('pageclass_sfx'); ?>">


    <h2 class="<?php echo $this->params->get('pageclass_sfx') ?>">
        <?php echo $this->escape($this->params->get('page_title')); ?>
    </h2>


    <?php if ((isset($this->leading) || isset($this->primary) || isset($this->secondary) || isset($this->links)) && (count($this->leading) || count($this->primary) || count($this->secondary) || count($this->links))): ?>
        <!-- Item list -->
        <div class="Projects-List">

            <?php if (isset($this->leading) && count($this->leading)): ?>
                <!-- Leading items -->

                <?php foreach ($this->leading as $key => $item): ?>

                    <?php

                    if ($key < $Max_Items) {


                        $this->item = $item;
                        echo $this->loadTemplate('item');
                    }

                    ?>

                <?php endforeach; ?>

            <?php endif; ?>

            <?php if (isset($this->primary) && count($this->primary)): ?>
                <div id="itemListPrimary">
                    <?php foreach ($this->primary as $key => $item): ?>

                        <?php
                        $this->item = $item;
                        echo $this->loadTemplate('item');
                        ?>


                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

            <?php if (isset($this->secondary) && count($this->secondary)): ?>
                <!-- Secondary items -->
                <div id="itemListSecondary">
                    <?php foreach ($this->secondary as $key => $item): ?>

                        <?php
                        // Define a CSS class for the last container on each row
                        if ((($key + 1) % ($this->params->get('num_secondary_columns')) == 0) || count($this->secondary) < $this->params->get('num_secondary_columns'))
                            $lastContainer = ' itemContainerLast';
                        else
                            $lastContainer = '';
                        ?>

                        <div
                            class="itemContainer<?php echo $lastContainer; ?>"<?php echo (count($this->secondary) == 1) ? '' : ' style="width:' . number_format(100 / $this->params->get('num_secondary_columns'), 1) . '%;"'; ?>>
                            <?php
                            // Load category_item.php by default
                            $this->item = $item;
                            echo $this->loadTemplate('item');
                            ?>
                        </div>
                        <?php if (($key + 1) % ($this->params->get('num_secondary_columns')) == 0): ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

            <?php if (isset($this->links) && count($this->links)): ?>
                <!-- Link items -->
                <div id="itemListLinks">
                    <h4><?php echo JText::_('K2_MORE'); ?></h4>
                    <?php foreach ($this->links as $key => $item): ?>

                        <?php
                        // Define a CSS class for the last container on each row
                        if ((($key + 1) % ($this->params->get('num_links_columns')) == 0) || count($this->links) < $this->params->get('num_links_columns'))
                            $lastContainer = ' itemContainerLast';
                        else
                            $lastContainer = '';
                        ?>

                        <div
                            class="itemContainer<?php echo $lastContainer; ?>"<?php echo (count($this->links) == 1) ? '' : ' style="width:' . number_format(100 / $this->params->get('num_links_columns'), 1) . '%;"'; ?>>
                            <?php
                            // Load category_item_links.php by default
                            $this->item = $item;
                            echo $this->loadTemplate('item_links');
                            ?>
                        </div>
                        <?php if (($key + 1) % ($this->params->get('num_links_columns')) == 0): ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

        </div>


        <!-- Pagination -->
        <?php if ($this->pagination->getPagesLinks()): ?>
            <div class="k2Pagination">
                <?php if ($this->params->get('catPagination')) echo $this->pagination->getPagesLinks(); ?>
                <?php if ($this->params->get('catPaginationResults')) echo $this->pagination->getPagesCounter(); ?>
            </div>
        <?php endif; ?>

    <?php endif; ?>
</div>
<!-- End K2 Category Layout -->

