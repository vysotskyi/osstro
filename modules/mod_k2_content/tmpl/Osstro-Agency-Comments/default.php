<?php defined('_JEXEC') or die; ?>

<section class="Comments Node-A">

    <?php foreach ($items as $key => $item){
        ?>

        <article class="Comment">
            <div class="Comment-Image">
                <img src="<?php echo $item->image; ?>">
            </div>
            <div class="Comment-Body">
                <h4 class="Comment-Author"><?= $item->title; ?></h4>
                <?php if (!empty(json_decode($item->extra_fields)[0]->value)) { ?>
                    <p class="Comment-Position"><?= json_decode($item->extra_fields)[0]->value; ?></p>
                <?php } ?>

                <div class="Comment-Content"><?= $item->introtext; ?></div>
            </div>
        </article>
    <?php } ?>
</section>
























