<?php
/**
 * @version        2.6.x
 * @package        K2
 * @author        JoomlaWorks http://www.joomlaworks.net
 * @copyright    Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license        GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

$URI = explode('?', $_SERVER['REQUEST_URI']);
$URI = preg_split('@/@', $URI[0], NULL, PREG_SPLIT_NO_EMPTY);

$Index = 0;

?>

<!-- Start K2 Category Layout -->
<div class="Projects Module-View">

    <div class="Projects-List">

        <?php foreach ($items as $key => $item): ?>
            <?php if ($item->link != '/' . $URI[1] && $Index<10) { ?>
                <?php if (json_decode($item->params)->catItemIntroText) { ?>
                    <!--                    <a class="Project Introtext">-->
                    <!--                        --><?php //echo $item->introtext; ?>
                    <!--                    </a>-->
                <?php } else { $Index++ ?>

                    <a class="Project" href="/projects<?php echo $item->link; ?>">
                        <h3>
                            <?php echo $item->title; ?>
                        </h3>


                        <div class="Project-Image">
                            <?php if (!empty($item->image)) { ?>
                                <img src="<?php echo $item->image; ?>">
                            <?php } ?>
                        </div>
                    </a>
                <?php } ?>
            <?php } ?>

        <?php endforeach; ?>
    </div>
</div>
<!-- End K2 Category Layout -->
